# runtime_impact_factor_assessment

After cloning the directory, the following steps have to be taken:

## Installing python3 and the Libraries Used
First, python3 needs to be installed. Furthermore, the libraries used (see clustering.py file) need to be installed (e.g., using pip)

## Executing the Code
After having set up everything correctly, the code can be executed (e.g., using the command "./clustering.py"). Three different files are provided:
  * clustering.py: creates figures based on ordering the traces for execution by choosing the most promising process instance
  * clustering_asc.py: creates figures based on ordering the traces for execution by choosing the least promising process instance
  * clustering.rand.py: creates figures based on ordering the traces for execution randomly 
