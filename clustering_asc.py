#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from sklearn.cluster import DBSCAN
from sklearn.neighbors import NearestNeighbors
from sklearn.metrics import silhouette_score
from sklearn.metrics import silhouette_samples
from scipy.spatial import distance_matrix
from numpy import unique
from operator import itemgetter
import copy
import random

print("clustering")

wd = "./data/"
data14 = pd.read_csv(wd+"results14.csv")
data15 = pd.read_csv(wd+"results15.csv")
data14_keyence = pd.read_csv(wd+"results14_keyence.csv")
data15_keyence = pd.read_csv(wd+"results15_keyence.csv")
data15_chipOccurrence = pd.read_csv(wd+"chipOccurrence_manuallyCreated.csv")

#data15.info()
#data15.plot(subplots=True)
#data15["aaLoad_X_wgtdAvg"].plot(subplots=True, figsize=(6, 6))
#plt.show()
#print(data15["aaLoad_X_wgtdAvg"])

d14_pre = data14.merge(data14_keyence,how="inner", left_on=["uuid","status","scale"], right_on=["uuid","status","scale"])
print("d14_pre:",len(d14_pre))
d15_prepre = data15.merge(data15_keyence,how="inner", left_on=["uuid","status","scale"], right_on=["uuid","status","scale"])
print("d15_prepre:",len(d15_prepre))
d15_pre = d15_prepre.merge(data15_chipOccurrence,how="inner", left_on=["uuid"], right_on=["uuid"])
print("d15_pre:",len(d15_pre))

d14_buffer = d14_pre[["uuid","status","scale","aaLoad_X_dpNumber","dpNumber"]]
d14_cleanAll = d14_buffer.dropna().merge(d14_pre,how="inner",left_on=["uuid","status","scale","aaLoad_X_dpNumber","dpNumber"],right_on=["uuid","status","scale","aaLoad_X_dpNumber","dpNumber"])
d14_buffer = d14_pre[["uuid","status","scale","aaLoad_X_dpNumber"]]
d14_cleanMachining = d14_buffer.dropna().merge(d14_pre,how="inner",left_on=["uuid","status","scale","aaLoad_X_dpNumber"],right_on=["uuid","status","scale","aaLoad_X_dpNumber"])
d14_buffer = d14_pre[["uuid","status","scale","dpNumber"]]
d14_cleanKeyence= d14_buffer.dropna().merge(d14_pre,how="inner",left_on=["uuid","status","scale","dpNumber"],right_on=["uuid","status","scale","dpNumber"])
print("d14_cleanAll:",len(d14_cleanAll))
print("d14_cleanMachining:",len(d14_cleanMachining))
print("d14_cleanKeyence:",len(d14_cleanKeyence))

d15_buffer = d15_pre[["uuid","status","scale","aaLoad_X_dpNumber","dpNumber","wgtdAvgSect4_1"]]
d15_cleanAll = d15_buffer.dropna().merge(d15_pre,how="inner",left_on=["uuid","status","scale","aaLoad_X_dpNumber","dpNumber","wgtdAvgSect4_1"],right_on=["uuid","status","scale","aaLoad_X_dpNumber","dpNumber","wgtdAvgSect4_1"])
d15_buffer = d15_pre[["uuid","status","scale","aaLoad_X_dpNumber"]]
d15_cleanMachining = d15_buffer.dropna().merge(d15_pre,how="inner",left_on=["uuid","status","scale","aaLoad_X_dpNumber"],right_on=["uuid","status","scale","aaLoad_X_dpNumber"])
d15_buffer = d15_pre[["uuid","status","scale","dpNumber","wgtdAvgSect4_1"]]
d15_cleanKeyence= d15_buffer.dropna().merge(d15_pre,how="inner",left_on=["uuid","status","scale","dpNumber","wgtdAvgSect4_1"],right_on=["uuid","status","scale","dpNumber","wgtdAvgSect4_1"])
print("d15_cleanAll:",len(d15_cleanAll))
print("d15_cleanMachining:",len(d15_cleanMachining))
print("d15_cleanKeyence:",len(d15_cleanKeyence))


d15_buffer = d15_pre[["uuid","status","scale","dpNumber","wgtdAvg","chip"]]
d15_buffer = d15_buffer[(d15_buffer["chip"] != "/") & (d15_buffer["chip"] != "?")]
d15_cleanKeyenceChip = d15_buffer.dropna().merge(d15_pre,how="inner",left_on=["uuid","status","scale","dpNumber","wgtdAvg","chip"],right_on=["uuid","status","scale","dpNumber","wgtdAvg","chip"])
print("d15_cleanKeyenceChip:",len(d15_cleanKeyenceChip))

d15_buffer = d15_pre[["uuid","status","scale","aaLoad_X_dpNumber","chip"]]
d15_buffer = d15_buffer[(d15_buffer["chip"] != "/") & (d15_buffer["chip"] != "?")]
d15_cleanMachiningChip = d15_buffer.dropna().merge(d15_pre,how="inner",left_on=["uuid","status","scale","aaLoad_X_dpNumber","chip"],right_on=["uuid","status","scale","aaLoad_X_dpNumber","chip"])
print("d15_cleanMachiningChip:",len(d15_cleanMachiningChip))

d15_chipless_threshold= d15_cleanAll[d15_cleanAll["wgtdAvg"] < 19]
print("d15_chipless_threshold:",len(d15_chipless_threshold))





wd2 = "./data/"
logistics_pre = pd.read_excel(wd2+"Logistic_Data2_used.xlsx")
#print(logistics_pre)
#print(len(logistics_pre))

measurements = logistics_pre.iloc[:,1:12]
measurements_first = logistics_pre.iloc[:,1:4]
measurements_second = logistics_pre.iloc[:,5:8]
measurements_third = logistics_pre.iloc[:,9:12]

logistics = pd.DataFrame({
"uuid":range(0,len(measurements)),
"group":logistics_pre.iloc[:,0],
"min":measurements.min(axis=1),
"max":measurements.max(axis=1),
"avg":measurements.sum(axis=1)/len(measurements.columns),
"first_part_min":measurements_first.min(axis=1),
"first_part_max":measurements_first.max(axis=1),
"first_part_avg":measurements_first.sum(axis=1)/len(measurements.columns),
"second_part_min":measurements_second.min(axis=1),
"second_part_max":measurements_second.max(axis=1),
"second_part_avg":measurements_second.sum(axis=1)/len(measurements.columns),
"third_part_min":measurements_third.min(axis=1),
"third_part_max":measurements_third.max(axis=1),
"third_part_avg":measurements_third.sum(axis=1)/len(measurements.columns)
})
#print(logistics)
print(f"logistics: {len(logistics)}")


def get_cluster_stats(array, dataset):#{{{
  result = {}
  for key, value in array.items():
    print(key)

    result[key] = {}
    thisresult = result[key]

    #data = dataset[[f"{i}min",f"{i}max",f"{i}avg",f"{i}wgtdAvg"]]
    selection = []
    for param in value['params']:
      selection.append(f"{key}{param}")
    print(selection)
    data = dataset[selection]
    status = dataset[value["goal"]["goal_param"]]

    model = DBSCAN(eps=value["eps"], min_samples=5)
    fit = model.fit_predict(data)
    single_sil = silhouette_samples(data, fit)
    sil = silhouette_score(data, fit)
    print(sum(single_sil)/len(data))
    print(sil)

    thisresult["sil"] = sil
    thisresult["clusters"] = fit 
    thisresult["uuids"] = dataset["uuid"].to_numpy()

    sil= {}
    number = {}
    statok = {}
    statnok = {}
    array_pos = []
    array_neg = []
    for cluster in zip(fit, single_sil, status):
      if cluster[0] in sil:
        sil[cluster[0]] += cluster[1]
        number[cluster[0]] += 1
        if cluster[2] == value["goal"]["pos_case"]:
          statok[cluster[0]] += 1
          array_pos.append(True)
          array_neg.append(False)
        if cluster[2] == value["goal"]["neg_case"]:
          statnok[cluster[0]] += 1
          array_pos.append(False)
          array_neg.append(True)
      else:
        sil[cluster[0]] = cluster[1]
        number[cluster[0]] = 1
        if cluster[2] == value["goal"]["pos_case"]:
          statok[cluster[0]] = 1
          statnok[cluster[0]] = 0
          array_pos.append(True)
          array_neg.append(False)
        if cluster[2] == value["goal"]["neg_case"]:
          statnok[cluster[0]] = 1
          statok[cluster[0]] = 0
          array_pos.append(False)
          array_neg.append(True)

    thisresult["array_pos"] = array_pos
    thisresult["array_neg"] = array_neg
    thisresult["clusterinfo"] = {}
    for c in sil:
      thisresult["clusterinfo"][c] = {}
      print(c,":",sil[c]/number[c])
      print(f"{value['goal']['pos_case']}: {statok[c]}, {value['goal']['neg_case']}: {statnok[c]}")

      thisresult["clusterinfo"][c]["sil"] = sil[c]/number[c]
      thisresult["clusterinfo"][c]["pos_case"] = statok[c]
      thisresult["clusterinfo"][c]["neg_case"] = statnok[c]

  return result
#}}}

def simulate(static_score):#{{{
  score = {}
  for uuid, resultate in zuordnungen.items():
    score[uuid] = {}
    #global = 0
    #local = 0
    global_values = []
    local_values = []
    counter = 0
    for cluster_id,resultat in resultate.items():
      #print(static_score[cluster_id])
      #global += static_score[cluster_id]["sil"]
      #local += static_score[cluster_id][cluster]["sil"]
      global_values.append(static_score[cluster_id]["sil"])
      local_values.append(static_score[cluster_id][resultat["cluster"]]["sil"])
      counter+=1
    #score[uuid]["global"] = (global/counter)
    score[uuid]["global"] = sum(global_values)/counter
    score[uuid]["global_min"] = min(global_values)
    score[uuid]["global_max"] = max(global_values)
    #score[uuid]["local"] = (local/counter)
    score[uuid]["local"] = sum(local_values)/counter
    score[uuid]["local_min"] = min(local_values)
    score[uuid]["local_max"] = max(local_values)
  
  #print(score)
  
  reihenfolge = []
  for uuid, value in score.items():
    #print(uuid, value["local"])
    reihenfolge.append({"uuid":uuid,"value":value["local"]})
  
  #reihenfolge_sortiert = sorted(reihenfolge,key=lambda x: x["value"])
  reihenfolge_sortiert = sorted(reihenfolge,key=lambda x: x["value"], reverse=True)
  #print(reihenfolge_sortiert)
  
  updating_score = copy.deepcopy(static_score)
  all_updates = []
  all_updates.append(copy.deepcopy(updating_score))
  #print(updating_score)
  for item in reihenfolge_sortiert:
    #print(item["uuid"])
    #print(zuordnungen[item["uuid"]])
    for param, resultate in zuordnungen[item["uuid"]].items():
      #print(updating_score[param][resultate["cluster"]])
      if(resultate["pos_case"]):
        updating_score[param][resultate["cluster"]]["pos_case"] += 1
      if(resultate["neg_case"]):
        updating_score[param][resultate["cluster"]]["neg_case"] += 1
    all_updates.append(copy.deepcopy(updating_score))
    #print(updating_score)
  
  #print(all_updates)
  #print(static_score)
  
  impact_series = {}
  for stat_score in all_updates:
    #print(stat_score)
    for param, paraminfo in stat_score.items():
      if(param not in impact_series):
        impact_series[param] = {}
      for index,clusterinfo in paraminfo.items():
        if(index != "sil"):
          if(index not in impact_series[param]):
            impact_series[param][index] = {}
          #print(param,index,clusterinfo)
          if("sil" not in impact_series[param][index]):
            impact_series[param][index]["sil"] = []
          impact_series[param][index]["sil"].append(clusterinfo["sil"])
          if("pos_case" not in impact_series[param][index]):
            impact_series[param][index]["pos_case"] = []
          impact_series[param][index]["pos_case"].append(clusterinfo["pos_case"])
          if("neg_case" not in impact_series[param][index]):
            impact_series[param][index]["neg_case"] = []
          impact_series[param][index]["neg_case"].append(clusterinfo["neg_case"])
  
  #print(impact_series)
  
  for k,v in impact_series.items():
    for ke,va in v.items():
      to_draw = []
      for z in zip(va["pos_case"],va["neg_case"],va["sil"]):
        if(z[0]+z[1]!=0):
          #to_draw.append((max(z[0],z[1])/(z[0]+z[1]))*(z[2]))
          #to_draw.append((max(z[0],z[1])/(z[0]+z[1]))*(z[2]+1))
          to_draw.append((max(z[0],z[1])/(z[0]+z[1]))*((z[2]+1)/2))
          ############to_draw.append((z[0]/(z[0]+z[1]))*((z[2]+1)/2)+(1-z[1]/(z[0]+z[1]))*((z[2]+1)/2))
          #to_draw.append((max(z[0],z[1])/(z[0]+z[1]))*(abs(z[2])))
        else:
          to_draw.append(0)
      plt.plot(range(1,len(to_draw)+1,1),to_draw, label=f"{k} - {ke}")
  
  plt.legend()
  plt.show()

  
  for k,v in impact_series.items():
    single_clusters = {}
    for ke,va in v.items():
      for z in zip(va["pos_case"],va["neg_case"],va["sil"]):
        if ke not in single_clusters:
          single_clusters[ke] = {}
          single_clusters[ke]["array"] = []
          single_clusters[ke]["cases"] = []
          single_clusters[ke]["sil"] = []
        single_clusters[ke]["cases"].append((z[0]+z[1]))
        #single_clusters[ke]["sil"].append(z[2])
        #single_clusters[ke]["sil"].append(max(0,z[2]))
        #single_clusters[ke]["sil"].append(z[2]+1)
        single_clusters[ke]["sil"].append((z[2]+1)/2)
        #single_clusters[ke]["sil"].append(abs(z[2]))
        if(z[0]+z[1]!=0):
          #single_clusters[ke]["array"].append((max(z[0],z[1])/(z[0]+z[1]))*(z[2]))
          #single_clusters[ke]["array"].append((max(z[0],z[1])/(z[0]+z[1]))*(max(0,z[2])))
          #single_clusters[ke]["array"].append((max(z[0],z[1])/(z[0]+z[1]))*(z[2]+1))
          single_clusters[ke]["array"].append((max(z[0],z[1])/(z[0]+z[1]))*((z[2]+1)/2))
          #####single_clusters[ke]["array"].append((z[0]/(z[0]+z[1]))*((z[2]+1)/2)+(1-z[1]/(z[0]+z[1]))*((z[2]+1)/2))
          #single_clusters[ke]["array"].append((max(z[0],z[1])/(z[0]+z[1]))*(abs(z[2])))
        else:
          single_clusters[ke]["array"].append(0)
    print(single_clusters)
    to_draw = []
    case_sum = []
    for cluster, clusterinfo in single_clusters.items():
      for index, value in enumerate(zip(clusterinfo["array"], clusterinfo["cases"], clusterinfo["sil"])):
        if index > len(to_draw)-1:
          to_draw.append(0)
          case_sum.append(0)
        #to_draw[index] += value[0]*value[2]
        #to_draw[index] += value[0]*value[1]
        to_draw[index] += value[0]*value[1]*value[2]
        #case_sum[index] += value[2]
        #case_sum[index] += value[1]
        case_sum[index] += value[1]*value[2]
    print(case_sum)
    for index, datapoint in enumerate(to_draw):
      #print(len(single_clusters.keys()))
      #to_draw[index] /= len(single_clusters)
      if(case_sum[index] != 0):
        to_draw[index] /= case_sum[index]
    #print(k,to_draw)
    plt.plot(range(1,len(to_draw)+1,1),to_draw, label=f"{k} ")
  
  plt.legend()
  plt.show()


  
  
  step = 0
  all_statistic = []
  all_impact= []
  for stat_score in all_updates:
    #print(stat_score)
    statistic = {"pos":[], "neg":[], "none":[]}
    for uuid,value in zuordnungen.items():
      #print(uuid)
      wert = 0
      counter = 0
      for param, resultate in value.items():
  
        #print(stat_score[param][resultate["cluster"]]["sil"])
        #print(stat_score[param][resultate["cluster"]]["pos_case"])
        #print(stat_score[param][resultate["cluster"]]["neg_case"])
        
        #beitrag_stat = (stat_score[param][resultate["cluster"]]["sil"])
        #beitrag_stat = (max(0,stat_score[param][resultate["cluster"]]["sil"]))
        #beitrag_stat = (stat_score[param][resultate["cluster"]]["sil"]+1)
        beitrag_stat = (stat_score[param][resultate["cluster"]]["sil"]+1)/2
        #beitrag_stat = abs(stat_score[param][resultate["cluster"]]["sil"])
        
        #beitrag_stat = possible_beitrag_stat
        #threshold = 1.7
        #if(beitrag_stat<threshold):
          #beitrag_stat = 0
        beitrag = 0
        if(stat_score[param][resultate["cluster"]]["pos_case"]>stat_score[param][resultate["cluster"]]["neg_case"]):
          beitrag_dyn = stat_score[param][resultate["cluster"]]["pos_case"]/(stat_score[param][resultate["cluster"]]["pos_case"]+stat_score[param][resultate["cluster"]]["neg_case"])
          beitrag = beitrag_stat*beitrag_dyn
          wert+=beitrag_stat*beitrag_dyn*1
          wert+=beitrag_stat*(1-beitrag_dyn)*0
        elif(stat_score[param][resultate["cluster"]]["pos_case"]<stat_score[param][resultate["cluster"]]["neg_case"]):
          beitrag_dyn = stat_score[param][resultate["cluster"]]["neg_case"]/(stat_score[param][resultate["cluster"]]["pos_case"]+stat_score[param][resultate["cluster"]]["neg_case"])
          beitrag = beitrag_stat*beitrag_dyn
          wert+=beitrag_stat*beitrag_dyn*0
          wert+=beitrag_stat*(1-beitrag_dyn)*1
        else:
          #undecided (dont add anything)
          #print("UNDECIDED")
          pass
 
        #counter += beitrag
        counter += beitrag_stat
      
      if(counter != 0):
        wert=wert/counter
      else:
        wert = -1
      if(resultate["pos_case"]):
        #print("pos",wert)
        statistic["pos"].append(wert)
      elif(resultate["neg_case"]):
        #print("neg",wert)
        statistic["neg"].append(wert)
      else:
        statistic["none"].append(wert)
    all_statistic.append(copy.deepcopy(statistic))
    step+=1
  
  #plt.boxplot(statistic["pos"],labels=["pos"],positions=[1])
  #plt.boxplot(statistic["neg"],labels=["neg"],positions=[2])
  #plt.show()
  
  #print(all_statistic[0])
  #print(all_statistic[1])
  #print(all_statistic[2])
  #print(all_statistic[3])
  #print(step)
  
  c = 1
  s = 5
  plt.boxplot(all_statistic[1]["pos"],labels=["pos1"],positions=[c])
  plt.boxplot(all_statistic[1]["neg"],labels=["neg1"],positions=[c+1])
  c += 2
  while (s < len(all_statistic)-1):
    plt.boxplot(all_statistic[s]["pos"],labels=[f"pos{s}"],positions=[c])
    plt.boxplot(all_statistic[s]["neg"],labels=[f"neg{s}"],positions=[c+1])
    s += 5
    c += 2
  
  plt.boxplot(all_statistic[len(all_statistic)-1]["pos"],labels=[f"pos{len(all_statistic)-1}"],positions=[c])
  plt.boxplot(all_statistic[len(all_statistic)-1]["neg"],labels=[f"neg{len(all_statistic)-1}"],positions=[c+1])
  
  plt.show()
#}}}

def simulate_reorder(static_score, zuordnungen, figure_name):#{{{

  #print(zuordnungen)
  this_step = copy.deepcopy(static_score)
  item = None
  to_draw_clusters = {}
  to_draw_ds= {}
  to_draw_instance_results = []
  #for i in range(0,15):
  #for i in range(-1,len(reihenfolge_sortiert),1):
  for i in range(-1,len(zuordnungen),1):
  #for i in range(-1,1,1):
 
    if(item != None):
      #print(static_score)
      for ds,dsinfo in zuordnungen[item["uuid"]].items():
        if(dsinfo["pos_case"]):
          this_step[ds][dsinfo["cluster"]]["pos_case"] += 1
        if(dsinfo["neg_case"]):
          this_step[ds][dsinfo["cluster"]]["neg_case"] += 1
    #print(this_step)
    

    #print(this_step)
    #print(item)
  
    assigned_clusters = {}
    #print(zuordnungen)
    for uuid,instanceinfo in zuordnungen.items():
      assigned_clusters[uuid] = {}
      for ds,dsinfo in instanceinfo.items():
        #print(ds, dsinfo)
        if(ds not in assigned_clusters[uuid]):
          assigned_clusters[uuid][ds] = {}
        assigned_clusters[uuid][ds]["assigned_cluster"] = dsinfo["cluster"]
        assigned_clusters[uuid][ds]["pos_case"] = dsinfo["pos_case"]
        assigned_clusters[uuid][ds]["neg_case"] = dsinfo["neg_case"]
    #print(assigned_clusters)
  
    # add values for drawing figure 1 (showing importance of individual clusters) and figure 2 (drawing importance of individual data streams)
    for ds,dsinfo in this_step.items():
      if(ds not in to_draw_clusters):
        to_draw_clusters[ds] = {}
      if(ds not in to_draw_ds):
        to_draw_ds[ds] = []
      ds_sum = 0
      ds_counter = 0
      for cluster,va in dsinfo.items():
        if cluster != "sil":
          if(cluster not in to_draw_clusters[ds]):
            to_draw_clusters[ds][cluster] = []
          #print(this_step[ds][cluster])
          pos_cases=this_step[ds][cluster]["pos_case"]
          neg_cases=this_step[ds][cluster]["neg_case"]
          ##used_sil=this_step[ds][cluster]["sil"]
          ##used_sil=(this_step[ds][cluster]["sil"]+1)
          used_sil=(this_step[ds][cluster]["sil"]+1)/2
          ##used_sil=abs(this_step[ds][cluster]["sil"])
          if(pos_cases+neg_cases != 0):
          #if(pos_cases != neg_cases):
            to_draw_clusters[ds][cluster].append((max(pos_cases,neg_cases)/(pos_cases+neg_cases))*used_sil)
          else:
            to_draw_clusters[ds][cluster].append(0)
          wert = to_draw_clusters[ds][cluster][-1]
          cases = pos_cases+neg_cases
          ##ds_sum += wert*used_sil
          ##ds_sum += wert*cases
          ds_sum += wert*cases*used_sil
          ##ds_counter += used_sil
          ##ds_counter += cases 
          ds_counter += cases*used_sil 
          #print(to_draw_clusters)
      to_draw_ds[ds].append(ds_sum)
      if(ds_counter != 0):
        to_draw_ds[ds][-1] /= ds_counter
    
    
  
    
    # add values for drawing figure 3 (overall scores for individual instances)
    pos = []
    neg = []
    none = []
    for uuid,instanceinfo in assigned_clusters.items():
      summe = 0
      counter = 0
      for ds,dsinfo in instanceinfo.items():
        ##used_sil = this_step[ds][dsinfo["assigned_cluster"]]["sil"]
        ##used_sil = (this_step[ds][dsinfo["assigned_cluster"]]["sil"]+1)
        used_sil = (this_step[ds][dsinfo["assigned_cluster"]]["sil"]+1)/2
        ##used_sil = abs(this_step[ds][dsinfo["assigned_cluster"]]["sil"])
        static_beitrag = used_sil
        pos_cases = this_step[ds][dsinfo["assigned_cluster"]]["pos_case"]
        neg_cases = this_step[ds][dsinfo["assigned_cluster"]]["neg_case"]
        dynamic_beitrag = 0
        if(pos_cases + neg_cases != 0):
        #if(pos_cases != neg_cases):
          dynamic_beitrag = pos_cases/(pos_cases+neg_cases)
        summe += static_beitrag * dynamic_beitrag
        counter += static_beitrag
      if(dsinfo["pos_case"]):
        pos.append(summe/counter)
      elif(dsinfo["neg_case"]):
        neg.append(summe/counter)
      else:
        none.append(summe/counter)
  
    to_draw_instance_results.append({"pos": pos, "neg": neg, "none": none})
  
    #print(to_draw_instance_results)
    

    #--------------------------------------------- reorder and set next item
    reihenfolge = []
    for uuid, resultate in zuordnungen.items():
      local_values = []
      counter = 0
      for cluster_id,resultat in resultate.items():
        #used_sil_local = this_step[cluster_id][resultat["cluster"]]["sil"]
        used_sil_local = (this_step[cluster_id][resultat["cluster"]]["sil"]+1)/2
        pos_cases = this_step[cluster_id][resultat["cluster"]]["pos_case"]
        neg_cases = this_step[cluster_id][resultat["cluster"]]["neg_case"]
        cases = pos_cases+neg_cases
        if(cases == 0):
        #if(True):
          local_values.append(used_sil_local)
          counter+=1
        else: 
          local_values.append(used_sil_local*cases)
          counter+=cases
      reihenfolge.append({"uuid":uuid,"value":sum(local_values)/counter})
    
    if(i != -1 and len(reihenfolge_sortiert) > 0):
      reihenfolge_buffer = []
      for to_add in reihenfolge_sortiert[1:]:
        for available in reihenfolge:
          if(to_add["uuid"] == available["uuid"]):
            reihenfolge_buffer.append(available)
      reihenfolge = reihenfolge_buffer
    #print(len(reihenfolge))

    #reihenfolge_sortiert = reihenfolge
    #random.seed(42+i)
    #random.shuffle(reihenfolge_sortiert)
    reihenfolge_sortiert = sorted(reihenfolge,key=lambda x: x["value"])
    #reihenfolge_sortiert = sorted(reihenfolge,key=lambda x: x["value"], reverse=True)

    if(len(reihenfolge_sortiert) > 0):
      item = copy.deepcopy(reihenfolge_sortiert[0])
      
    



  # figure 1
  for ds,dsinfo in to_draw_clusters.items():
    for cluster,clusterscores in dsinfo.items():
      #print(clusterinfo)
      plt.plot(range(1,len(clusterscores)+1,1),clusterscores, label=f"{ds} - {cluster}")
  plt.legend()
  #plt.title('some title')
  plt.xlabel('Number of Finished Process Instances')
  plt.ylabel('Influence Score')
  plt.savefig(f"{figure_name}_entwicklung_cluster.svg")
  plt.show()
  
  # figure 2
  for ds,dsscores in to_draw_ds.items():
    #print(dsinfo)
    if(ds == ""):
      if(figure_name == "logistics"):
        plt.plot(range(1,len(dsscores)+1,1),dsscores, label=f"complete")
      else:
        plt.plot(range(1,len(dsscores)+1,1),dsscores, label=f"imprecise_measurement")
    else:
      plt.plot(range(1,len(dsscores)+1,1),dsscores, label=f"{ds} ")
  plt.legend()
  #plt.title('some title')
  plt.xlabel('Number of Finished Process Instances')
  plt.ylabel('Influence Score')
  plt.savefig(f"{figure_name}_entwicklung_ds.svg")
  plt.show()

  # figure 3
  c = 1
  s = 5
  p = []
  n = []
  p.append(plt.boxplot(to_draw_instance_results[1]["pos"],labels=["1"],positions=[c]))
  n.append(plt.boxplot(to_draw_instance_results[1]["neg"],labels=["1"],positions=[c+1]))
  c += 2
  while (s < len(to_draw_instance_results)-1):
    p.append(plt.boxplot(to_draw_instance_results[s]["pos"],labels=[f"{s}"],positions=[c]))
    n.append(plt.boxplot(to_draw_instance_results[s]["neg"],labels=[f"{s}"],positions=[c+1]))
    s += 5
    c += 2
  
  p.append(plt.boxplot(to_draw_instance_results[len(to_draw_instance_results)-1]["pos"],labels=[f"{len(to_draw_instance_results)-1}"],positions=[c]))
  n.append(plt.boxplot(to_draw_instance_results[len(to_draw_instance_results)-1]["neg"],labels=[f"{len(to_draw_instance_results)-1}"],positions=[c+1]))
 
  for el in p:
    for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
      plt.setp(el[element], color="green")
  for el in n:
    for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
      plt.setp(el[element], color="red")

  #plt.title('some title')
  plt.xlabel('Number of Finished Process Instances')
  plt.ylabel('Overall Score')
  plt.savefig(f"{figure_name}_score.svg")
  plt.show()



#}}}

standard_params = ["min","max","avg","wgtdAvg"]
chip_goal = {"goal_param": "chip", "pos_case": "no", "neg_case":"yes"}
status_goal = {"goal_param": "status", "pos_case": "ok", "neg_case":"nok"}

zuordnungen = {}
print("KEYENCE FOR CHIP OCCURRENCE (batch15)")
to_do = {"": {"params": standard_params , "eps": 0.72, "goal": chip_goal}}
res0 = get_cluster_stats(to_do, d15_cleanKeyenceChip)
#print(res0)

for key,value in res0.items():
  for c in zip(value["uuids"],value["clusters"],value["array_pos"],value["array_neg"]):
    if not(c[0] in zuordnungen):
      zuordnungen[c[0]] = {}
    zuordnungen[c[0]][key] = {"cluster":c[1],"pos_case":c[2],"neg_case":c[3]}

print("MACHINING FOR CHIP OCCURRENCE (batch15)")
to_do = {
"aaLoad_X_": {"params": standard_params, "eps": 1.81, "goal": chip_goal},
"aaLoad_Y_": {"params": standard_params, "eps": 0.17, "goal": chip_goal},
"aaLoad_Z_": {"params": standard_params, "eps": 1.2, "goal": chip_goal},
"aaVactB_X_": {"params": standard_params, "eps": 115, "goal": chip_goal},
"aaVactB_Y_": {"params": standard_params, "eps": 9.3, "goal": chip_goal},
"aaVactB_Z_": {"params": standard_params, "eps": 76.1, "goal": chip_goal},
"actSpeed_": {"params": standard_params, "eps": 4.9, "goal": chip_goal},
"driveLoad_": {"params": standard_params, "eps": 0.23, "goal": chip_goal}
}
res1 = get_cluster_stats(to_do, d15_cleanMachiningChip)
#print(res1)

for key,value in res1.items():
  for c in zip(value["uuids"],value["clusters"],value["array_pos"],value["array_neg"]):
    if not(c[0] in zuordnungen):
      zuordnungen[c[0]] = {}
    zuordnungen[c[0]][key] = {"cluster":c[1],"pos_case":c[2],"neg_case":c[3]}

print(zuordnungen)

static_score = {}
for key,value in res0.items():
  static_score[key] = {}
  for cluster,clusterinfo in value["clusterinfo"].items():
    static_score[key]["sil"] = value["sil"]
    static_score[key][cluster] = {}
    static_score[key][cluster]["sil"] = clusterinfo["sil"]
    static_score[key][cluster]["pos_case"] = 0
    static_score[key][cluster]["neg_case"] = 0


for key,value in res1.items():
  static_score[key] = {}
  for cluster,clusterinfo in value["clusterinfo"].items():
    static_score[key]["sil"] = value["sil"]
    static_score[key][cluster] = {}
    static_score[key][cluster]["sil"] = clusterinfo["sil"]
    static_score[key][cluster]["pos_case"] = 0
    static_score[key][cluster]["neg_case"] = 0

#simulate(static_score)
simulate_reorder(static_score, zuordnungen, "d15_chip")

zuordnungen={}
print("KEYENCE FOR TEST PREDICTION (batch15)")
to_do = {"": {"params": ["min","max","avg","wgtdAvgSect4_1"], "eps": 0.80, "goal": status_goal}}
res2 = get_cluster_stats(to_do, d15_chipless_threshold)
#print(res2)

for key,value in res2.items():
  for c in zip(value["uuids"],value["clusters"],value["array_pos"],value["array_neg"]):
    if not(c[0] in zuordnungen):
      zuordnungen[c[0]] = {}
    zuordnungen[c[0]][key] = {"cluster":c[1],"pos_case":c[2],"neg_case":c[3]}


print("MACHINING FOR TEST PREDICTION (batch15)")
to_do = {
"aaLoad_X_": {"params": standard_params, "eps": 2.72, "goal": status_goal},
"aaLoad_Y_": {"params": standard_params, "eps": 0.13, "goal": status_goal},
"aaLoad_Z_": {"params": standard_params, "eps": 1.36, "goal": status_goal},
"aaVactB_X_": {"params": standard_params, "eps": 2285.15, "goal": status_goal},
"aaVactB_Y_": {"params": standard_params, "eps": 10.77, "goal": status_goal},
"aaVactB_Z_": {"params": standard_params, "eps": 72.32, "goal": status_goal},
"actSpeed_": {"params": standard_params, "eps": 4.67, "goal": status_goal},
"driveLoad_": {"params": standard_params, "eps": 0.21, "goal": status_goal}
}
res3 = get_cluster_stats(to_do, d15_chipless_threshold)
#print(res3)

for key,value in res3.items():
  for c in zip(value["uuids"],value["clusters"],value["array_pos"],value["array_neg"]):
    if not(c[0] in zuordnungen):
      zuordnungen[c[0]] = {}
    zuordnungen[c[0]][key] = {"cluster":c[1],"pos_case":c[2],"neg_case":c[3]}

print(zuordnungen)

static_score = {}
for key,value in res2.items():
  static_score[key] = {}
  for cluster,clusterinfo in value["clusterinfo"].items():
    static_score[key]["sil"] = value["sil"]
    static_score[key][cluster] = {}
    static_score[key][cluster]["sil"] = clusterinfo["sil"]
    static_score[key][cluster]["pos_case"] = 0
    static_score[key][cluster]["neg_case"] = 0


for key,value in res3.items():
  static_score[key] = {}
  for cluster,clusterinfo in value["clusterinfo"].items():
    static_score[key]["sil"] = value["sil"]
    static_score[key][cluster] = {}
    static_score[key][cluster]["sil"] = clusterinfo["sil"]
    static_score[key][cluster]["pos_case"] = 0
    static_score[key][cluster]["neg_case"] = 0

#simulate(static_score)
simulate_reorder(static_score, zuordnungen, "d15_test")

zuordnungen = {}
print("MACHINING FOR TEST PREDICTION (batch14)")
to_do = {
"aaLoad_X_": {"params": standard_params, "eps": 2.72, "goal": status_goal},
"aaLoad_Y_": {"params": standard_params, "eps": 0.13, "goal": status_goal},
"aaLoad_Z_": {"params": standard_params, "eps": 1.36, "goal": status_goal},
"aaVactB_X_": {"params": standard_params, "eps": 2285.15, "goal": status_goal},
"aaVactB_Y_": {"params": standard_params, "eps": 10.77, "goal": status_goal},
"aaVactB_Z_": {"params": standard_params, "eps": 72.32, "goal": status_goal},
"actSpeed_": {"params": standard_params, "eps": 4.67, "goal": status_goal},
"driveLoad_": {"params": standard_params, "eps": 0.21, "goal": status_goal}
}
res4 = get_cluster_stats(to_do, d14_cleanMachining)
#print(res4)

for key,value in res4.items():
  for c in zip(value["uuids"],value["clusters"],value["array_pos"],value["array_neg"]):
    if not(c[0] in zuordnungen):
      zuordnungen[c[0]] = {}
    zuordnungen[c[0]][key] = {"cluster":c[1],"pos_case":c[2],"neg_case":c[3]}

print(zuordnungen)

static_score = {}
for key,value in res4.items():
  static_score[key] = {}
  for cluster,clusterinfo in value["clusterinfo"].items():
    static_score[key]["sil"] = value["sil"]
    static_score[key][cluster] = {}
    static_score[key][cluster]["sil"] = clusterinfo["sil"]
    static_score[key][cluster]["pos_case"] = 0
    static_score[key][cluster]["neg_case"] = 0

#simulate(static_score)
simulate_reorder(static_score, zuordnungen, "d14_test")



logistics_params = ["min","max","avg"]
logistics_goal = {"goal_param": "group", "pos_case": "normal", "neg_case":"except"}

zuordnungen = {}
print("LOGISTICS")
to_do = {
"": {"params": logistics_params, "eps": 1.87, "goal": logistics_goal},
"first_part_": {"params": logistics_params, "eps": 1.87, "goal": logistics_goal},
"second_part_": {"params": logistics_params, "eps": 1.87, "goal": logistics_goal},
"third_part_": {"params": logistics_params, "eps": 1.87, "goal": logistics_goal}
}
res5 = get_cluster_stats(to_do, logistics)
#print(res5)

for key,value in res5.items():
  for c in zip(value["uuids"],value["clusters"],value["array_pos"],value["array_neg"]):
    if not(c[0] in zuordnungen):
      zuordnungen[c[0]] = {}
    zuordnungen[c[0]][key] = {"cluster":c[1],"pos_case":c[2],"neg_case":c[3]}

#print(zuordnungen)
static_score = {}
for key,value in res5.items():
  static_score[key] = {}
  for cluster,clusterinfo in value["clusterinfo"].items():
    static_score[key]["sil"] = value["sil"]
    static_score[key][cluster] = {}
    static_score[key][cluster]["sil"] = clusterinfo["sil"]
    static_score[key][cluster]["pos_case"] = 0
    static_score[key][cluster]["neg_case"] = 0

#simulate(static_score)
simulate_reorder(static_score, zuordnungen, "logistics")

